server "dev.plusmedia.ru", user: 'master', port: 5430, roles: %w{web app db}

after 'deploy:publishing', 'deploy:restart'
namespace :deploy do
  task :restart do
    invoke 'unicorn:restart'
  end
end
