# -*- encoding : utf-8 -*-

Rails.application.routes.draw do
  # /login, /logout
  devise_for :users, path: '/', path_names: { sign_in: 'login', sign_out: 'logout' }, controllers: { sessions: 'sessions' }
  # /login, /logout

  # api/v1
  get 'api/v1/auth' => 'api/v1/auth#index'
  get 'api/v1/sections' => 'api/v1/sections#index'
  get 'api/v1/section_file/:id/translations' => 'api/v1/section_file#translations'
  get 'api/v1/section/:id/files' => 'api/v1/section#files'
  post 'api/v1/translations/:id/toggle_approved' => 'api/v1/translations#toggle_approved'
  # api/v1

  scope :section do
    get ':id/translations' => 'section#translations', as: :section_translations
    get ':id/drop_non_actual' => 'section#drop_non_actual', as: :section_drop_non_actual
  end

  scope :section_file do
    get ':id/translations' => 'section_file#translations', as: :section_file_translations
    get ':id/drop_non_actual' => 'section_file#drop_non_actual', as: :section_file_drop_non_actual
  end

  scope :translation do
    get ':id/approve' => 'translation#approve', as: :translation_approve
  end

  root :to => 'root#index'
end
