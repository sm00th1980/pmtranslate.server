url = 'redis://redis:6379'
namespace = 'pmtranslate'

Sidekiq.configure_server do |config|
  config.redis = { url: url, namespace: namespace }
end

Sidekiq.configure_client do |config|
  config.redis = { url: url, namespace: namespace }
end
