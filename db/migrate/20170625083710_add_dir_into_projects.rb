# -*- encoding : utf-8 -*-

class AddDirIntoProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :dir, :string
  end
end
