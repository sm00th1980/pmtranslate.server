# -*- encoding : utf-8 -*-

class AddActualIntoSectionFiles < ActiveRecord::Migration[5.1]
  def change
    add_column :section_files, :actual, :boolean, null: false, default: true
  end
end
