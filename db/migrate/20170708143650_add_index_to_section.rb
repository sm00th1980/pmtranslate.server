# -*- encoding : utf-8 -*-

class AddIndexToSection < ActiveRecord::Migration[5.1]
  def change
    add_index :sections, [:project_id]
  end
end
