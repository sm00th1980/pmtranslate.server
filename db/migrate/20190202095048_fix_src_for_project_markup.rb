class FixSrcForProjectMarkup < ActiveRecord::Migration[5.1]
  def up
    project = Project.first
    project.subdir = 'src/screen'
    project.save!
  end
end
