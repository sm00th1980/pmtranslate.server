# -*- encoding : utf-8 -*-

class AddApprovedIntoTranslations < ActiveRecord::Migration[5.1]
  def change
    add_column :translations, :approved, :boolean, null: false, default: false
  end
end
