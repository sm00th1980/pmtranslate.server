# -*- encoding : utf-8 -*-

class AddActualIntoSections < ActiveRecord::Migration[5.1]
  def change
    add_column :sections, :actual, :boolean, null: false, default: true
  end
end
