# -*- encoding : utf-8 -*-

class AddUniqIndexIntoProjects < ActiveRecord::Migration[5.1]
  def up
    add_index :projects, [:name], unique: true
  end
end
