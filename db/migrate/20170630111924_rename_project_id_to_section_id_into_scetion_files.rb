# -*- encoding : utf-8 -*-

class RenameProjectIdToSectionIdIntoScetionFiles < ActiveRecord::Migration[5.1]
  def change
    rename_column :section_files, :project_id, :section_id
  end
end
