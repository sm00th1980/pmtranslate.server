# -*- encoding : utf-8 -*-

class AddActualIntoTranslations < ActiveRecord::Migration[5.1]
  def change
    add_column :translations, :actual, :boolean, null: false, default: true
  end
end
