class AddFkToTranslations < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :translations, :section_files, column: :file_id, primary_key: :id
  end
end
