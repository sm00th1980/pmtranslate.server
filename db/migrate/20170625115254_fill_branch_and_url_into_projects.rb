# -*- encoding : utf-8 -*-

class FillBranchAndUrlIntoProjects < ActiveRecord::Migration[5.1]
  def up
    project = Project.last
    project.branch = 'master'
    project.url = 'git@bitbucket.org:pmcloud/gorko.r.git'

    project.save!
  end
end
