# -*- encoding : utf-8 -*-

class RenameProjectFileIdToFileIdIntoTranslations < ActiveRecord::Migration[5.1]
  def change
    rename_column :translations, :project_file_id, :file_id
  end
end
