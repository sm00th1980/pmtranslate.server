# -*- encoding : utf-8 -*-

class AddGorkoMarkupProject < ActiveRecord::Migration[5.1]
  def up
    Project.create!(name: "gorko.markup", dir: File.join('tmp', 'projects'), subdir: 'src', branch: 'master', url: 'git@bitbucket.org:pmcloud/gorko.markup.git')
  end
end
