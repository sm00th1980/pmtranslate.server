# -*- encoding : utf-8 -*-

class AddSampleUser < ActiveRecord::Migration[5.1]
  def up
    User.delete_all

    User.create!({ email: "support@gorko.plusmedia.ru", password: "12345678", password_confirmation: "12345678" })
    end

  def down
    User.delete_all
  end
end
