# -*- encoding : utf-8 -*-

class CreateSections < ActiveRecord::Migration[5.1]
  def change
    create_table :sections do |t|
      t.integer :project_id, null: false
      t.string :name, null: false

      t.timestamps
    end
  end
end
