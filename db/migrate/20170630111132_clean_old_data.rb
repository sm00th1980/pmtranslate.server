# -*- encoding : utf-8 -*-

class CleanOldData < ActiveRecord::Migration[5.1]
  def up
    Translation.delete_all
  end
end
