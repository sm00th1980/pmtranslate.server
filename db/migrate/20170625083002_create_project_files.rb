# -*- encoding : utf-8 -*-

class CreateProjectFiles < ActiveRecord::Migration[5.1]
  def change
    create_table :project_files do |t|
      t.integer :project_id, null: false
      t.string :name, null: false

      t.timestamps
    end
  end
end
