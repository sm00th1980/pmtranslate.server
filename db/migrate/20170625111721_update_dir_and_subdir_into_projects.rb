# -*- encoding : utf-8 -*-

class UpdateDirAndSubdirIntoProjects < ActiveRecord::Migration[5.1]
  def up
    project = Project.last
    project.dir = File.join('tmp', 'projects')
    project.subdir = 'src'

    project.save!
  end
end
