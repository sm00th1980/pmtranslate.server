class FillSections < ActiveRecord::Migration[5.1]
  def up
    Project.all.each do |project|
      RepoService.refresh(project)
      TranslationService.refresh(project)
    end
  end
end
