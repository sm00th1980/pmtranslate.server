# -*- encoding : utf-8 -*-

class CleanOldProjects < ActiveRecord::Migration[5.1]
  def up
    Translation.delete_all
    Project.delete_all
  end
end
