class CleanOldSections < ActiveRecord::Migration[5.1]
  def up
    Section.destroy_all
  end
end
