# -*- encoding : utf-8 -*-

class AddProjectFileIdIntoTranslation < ActiveRecord::Migration[5.1]
  def change
    add_column :translations, :project_file_id, :integer
  end
end
