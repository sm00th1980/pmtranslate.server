# -*- encoding : utf-8 -*-

class UpdateProjectDir < ActiveRecord::Migration[5.1]
  def up
    Project.all.each do |project|
      project.dir = 'tmp/projects/gorko.r/src'
      project.save!
    end
  end
end
