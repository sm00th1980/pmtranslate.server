# -*- encoding : utf-8 -*-

class AddIndexToSectionFile < ActiveRecord::Migration[5.1]
  def change
    add_index :section_files, [:section_id]
  end
end
