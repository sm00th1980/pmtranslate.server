# -*- encoding : utf-8 -*-

class AddIndexToTranslation < ActiveRecord::Migration[5.1]
  def change
    add_index :translations, [:file_id]
  end
end
