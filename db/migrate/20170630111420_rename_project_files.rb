# -*- encoding : utf-8 -*-

class RenameProjectFiles < ActiveRecord::Migration[5.1]
  def change
    rename_table :project_files, :section_files
  end
end
