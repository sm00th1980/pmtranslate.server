# -*- encoding : utf-8 -*-

class AddBranchAndUrlIntoProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :branch, :string
    add_column :projects, :url, :string
  end
end
