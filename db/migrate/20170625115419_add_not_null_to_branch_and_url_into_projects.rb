# -*- encoding : utf-8 -*-

class AddNotNullToBranchAndUrlIntoProjects < ActiveRecord::Migration[5.1]
  def up
    change_column :projects, :branch, :string, null: false
    change_column :projects, :url, :string, null: false
  end
end
