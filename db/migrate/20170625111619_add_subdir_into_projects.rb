# -*- encoding : utf-8 -*-

class AddSubdirIntoProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :subdir, :string, null: false, default: ''
  end
end
