class AddFkToSections < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :sections, :projects
  end
end
