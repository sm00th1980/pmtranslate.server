# -*- encoding : utf-8 -*-

class CreateTranslations < ActiveRecord::Migration[5.1]
  def change
    create_table :translations do |t|
      t.string :content, null: false, default: ""

      t.timestamps
    end
  end
end
