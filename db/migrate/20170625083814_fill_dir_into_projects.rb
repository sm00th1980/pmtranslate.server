# -*- encoding : utf-8 -*-

class FillDirIntoProjects < ActiveRecord::Migration[5.1]
  def up
    Project.all.each do |project|
      project.dir = 'gorko.r/src'
      project.save!
    end
  end
end
