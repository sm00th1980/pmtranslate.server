class AddFkToSectionFiles < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :section_files, :sections
  end
end
