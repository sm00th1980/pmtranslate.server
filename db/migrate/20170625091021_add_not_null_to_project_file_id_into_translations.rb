# -*- encoding : utf-8 -*-

class AddNotNullToProjectFileIdIntoTranslations < ActiveRecord::Migration[5.1]
  def up
    change_column :translations, :project_file_id, :integer, null: false
  end
end
