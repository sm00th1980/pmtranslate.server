# -*- encoding : utf-8 -*-

class AddNotNullToDirIntoProjects < ActiveRecord::Migration[5.1]
  def up
    change_column :projects, :dir, :string, null: false
  end
end
