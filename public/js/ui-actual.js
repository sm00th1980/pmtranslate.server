+function ($) {

	$(function(){
		var postApprove = function(id, approve) {
			$.get("/translation/" + id + "/approve", {approve: approve}, function(data) {

			});
		};

		var throttledClick = $.throttle(250, function(e) {
			var $label = $(e.target).closest('.i-switch');
			var $row = $label.closest('.js-row');

			var translationID = $label.data('translation-id');

			$row.toggleClass('text-muted');
			var approve = $row.hasClass('text-muted');

			postApprove(translationID, approve);
		}, true);

		//subscribe
		$(document).on('click', '.i-switch', throttledClick);
	});
}(jQuery);
