FROM ruby:2.4.1
RUN apt-get update -qq && apt-get install -y build-essential mc redis-tools libpq-dev cron nodejs

RUN unlink /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Samara /etc/localtime

# Make ssh dir
RUN mkdir /root/.ssh/

# Copy over private key, and set permissions
ADD id_rsa /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa

RUN echo "    StrictHostKeyChecking no" >> /etc/ssh/ssh_config

RUN mkdir /app
WORKDIR /app
ADD Gemfile /app/Gemfile
ADD Gemfile.lock /app/Gemfile.lock
RUN bundle install
ADD . /app
RUN whenever -i
