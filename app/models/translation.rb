# frozen_string_literal: true

class Translation < ApplicationRecord
  belongs_to :file, class_name: 'SectionFile'
  scope :actual, -> { where(actual: true) }
  scope :non_actual, -> { where(actual: false) }

  def toggle_approved!
    update(approved: !approved)
  end

  def repeat_count
    Translation.where(content: content).count
  end

  def created
    created_at.strftime('%Y-%m-%d %H:%M:%S')
  end

  def to_api
    {
      id: id,
      content: content,
      file_id: file_id,
      approved: approved,
      repeat_count: repeat_count,
      created_at: created_at.to_i
    }
  end
end
