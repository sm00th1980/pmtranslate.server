# frozen_string_literal: true

class Section < ApplicationRecord
  belongs_to :project
  has_many :files, class_name: 'SectionFile', dependent: :destroy

  scope :actual, -> { where(actual: true) }

  def non_actual_translations?
    files.select(&:non_actual_translations?).count.positive?
  end

  def to_api
    {
      id: id,
      name: name.split('___').last,
      files_count: files.size,
      translations_count: files.map(&:translations_total).sum
    }
  end
end
