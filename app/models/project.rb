# frozen_string_literal: true

class Project < ApplicationRecord
  has_many :sections, dependent: :destroy

  def source_path
    Rails.root.join(dir, name, subdir)
  end

  def path
    Rails.root.join(dir, name)
  end
end
