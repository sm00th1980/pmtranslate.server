# frozen_string_literal: true

class SectionFile < ApplicationRecord
  belongs_to :section
  has_many :translations, foreign_key: 'file_id', dependent: :destroy, inverse_of: :file

  scope :actual, -> { where(actual: true) }

  def full_path
    "#{section.name}/#{name}"
  end

  def translations_total
    translations.size
  end

  def translations_unique_total
    translations.map { |t| t.content.mb_chars.downcase.to_s }.uniq.count
  end

  def non_actual_translations?
    translations.non_actual.count.positive?
  end

  def to_api
    {
      id: id,
      name: name,
      section_id: section_id,
      translations: translations.map(&:to_api)
    }
  end
end
