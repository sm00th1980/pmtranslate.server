# frozen_string_literal: true

class SectionsDecorator < ApplicationDecorator
  delegate_all

  def li_class(section)
    model.id == section.id ? 'active' : ''
  end
end
