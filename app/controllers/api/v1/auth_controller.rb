# frozen_string_literal: true

require 'digest'

class Api::V1::AuthController < ApiController
  before_action :find_user!
  before_action :password_valid!

  def index
    render json: {
      success: true,
      access_token: Digest::MD5.hexdigest(@user.encrypted_password)
    }
  end

  private

  def auth_params
    params.permit(:username, :password)
  end

  def find_user!
    @user = User.find_by(email: auth_params[:username])
    render json: { success: false, error: I18n.t('auth.failure.user_not_found') } if @user.blank?
  end

  def password_valid!
    render json: { success: false, error: I18n.t('auth.failure.user_not_found') } unless @user.valid_password?(auth_params[:password])
  end
end
