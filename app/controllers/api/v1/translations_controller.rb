# frozen_string_literal: true

class Api::V1::TranslationsController < ApiController
  before_action :find_user!
  before_action :access_token_valid!
  before_action :find_translation!

  def toggle_approved
    @translation.toggle_approved!

    render json: {
      success: true,
      translation: @translation.reload.to_api
    }
  end

  private

  def translations_params
    params.permit(:username, :access_token, :id)
  end

  def find_user!
    @user = User.find_by(email: translations_params[:username])
    render json: { success: false, error: I18n.t('auth.failure.user_not_found') } if @user.blank?
  end

  def access_token_valid!
    access_token = Digest::MD5.hexdigest(@user.encrypted_password)
    render json: { success: false, error: I18n.t('auth.failure.user_not_found') } if access_token != translations_params[:access_token]
  end

  def find_translation!
    @translation = Translation.find_by(id: translations_params[:id])
    render json: { success: false, error: I18n.t('translations.failure.translation_not_found') } if @translation.blank?
  end
end
