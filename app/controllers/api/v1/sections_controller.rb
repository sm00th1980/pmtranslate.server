# frozen_string_literal: true

class Api::V1::SectionsController < ApiController
  def index
    sections = Section.all.sort_by { |section| section.name.split('___').first.to_i }
    render json: {
      success: true,
      sections: sections.map(&:to_api)
    }
  end
end
