# frozen_string_literal: true

class Api::V1::SectionFileController < ApiController
  before_action :find_section_file!

  def translations
    render json: {
      success: true,
      translations: @section_file.translations.map(&:to_api)
    }
  end

  private

  def section_file_params
    params.permit(:id)
  end

  def find_section_file!
    @section_file = SectionFile.find_by(id: section_file_params[:id])
    render json: { success: false, error: I18n.t('file.failure.not_found') } if @section_file.blank?
  end
end
