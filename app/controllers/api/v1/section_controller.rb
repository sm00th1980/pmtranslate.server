# frozen_string_literal: true

class Api::V1::SectionController < ApiController
  before_action :find_section!

  def files
    render json: {
      success: true,
      files: @section.files.map(&:to_api)
    }
  end

  private

  def section_params
    params.permit(:id)
  end

  def find_section!
    @section = Section.find_by(id: section_params[:id])
    render json: { success: false, error: I18n.t('section.failure.not_found') } if @section.blank?
  end
end
