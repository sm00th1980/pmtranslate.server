# frozen_string_literal: true

class RootController < ApplicationController
  before_action :check_user!

  def index
    first_file = Section.actual.first.files.actual.first
    redirect_to section_file_translations_path(first_file)
  end

  private

  def check_user!
    redirect_to new_user_session_path unless user_signed_in?
  end
end
