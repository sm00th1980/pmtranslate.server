# frozen_string_literal: true

class AuthController < ApplicationController
  include Controllers::ApplicationHelper

  before_action :authenticate_user!

  def authenticate_user!
    return if current_user.present?

    sign_out current_user
    flash[:alert] = t('authorization_is_required')
    redirect_to new_user_session_path
  end
end
