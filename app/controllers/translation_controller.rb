# frozen_string_literal: true

class TranslationController < AuthController
  before_action :find_translation!, only: %i[approve]

  def approve
    @translation.update(approved: parse_property(translation_params[:approve]))
    render json: { success: true }
  end

  private

  def translation_params
    params.permit(:id, :approve)
  end

  def find_translation!
    @translation = Translation.find_by(id: translation_params[:id])
    render json: { success: false, error: I18n.t('translation.failure.not_found') } if @translation.blank?
  end

  def parse_property(property)
    property.to_s.casecmp('true').zero?
  end
end
