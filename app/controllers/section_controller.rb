# frozen_string_literal: true

class SectionController < AuthController
  before_action :find_section!, only: %i[translations drop_non_actual]

  def drop_non_actual
    @section.files.each do |section_file|
      section_file.translations.non_actual.destroy_all if section_file.non_actual_translations?
    end

    redirect_to section_translations_path(@section), notice: I18n.t('section.drop_non_actual.success')
  end

  def translations
    @sections = SectionsDecorator.decorate_collection(Section.actual)
  end

  private

  def section_params
    params.permit(:id)
  end

  def find_section!
    @section = Section.find_by(id: section_params[:id])
    return if @section.present?

    first_actual_section = Section.actual.first
    redirect_to section_translations_path(first_actual_section), alert: I18n.t('section.failure.not_found')
  end
end
