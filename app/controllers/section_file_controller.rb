# frozen_string_literal: true

class SectionFileController < AuthController
  before_action :find_file!, only: %i[translations drop_non_actual]

  def drop_non_actual
    @file.translations.non_actual.destroy_all
    redirect_to section_file_translations_path(@file), notice: I18n.t('file.drop_non_actual.success')
  end

  def translations
    @section = @file.section
    @sections = SectionsDecorator.decorate_collection(Section.actual)
    @actual_translations = TranslationsDecorator.decorate_collection(@file.translations.actual)
    @non_actual_translations = TranslationsDecorator.decorate_collection(@file.translations.non_actual)
  end

  private

  def section_file_params
    params.permit(:id)
  end

  def find_file!
    @file = SectionFile.find_by(id: section_file_params[:id])
    return if @file.present?

    first_file = SectionFile.actual.first
    redirect_to section_file_translations_path(first_file), alert: I18n.t('file.failure.not_found')
  end
end
