# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Controllers::ApplicationHelper
  protect_from_forgery with: :exception
end
