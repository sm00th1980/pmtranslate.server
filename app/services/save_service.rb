# frozen_string_literal: true

def to_non_actual(records)
  records.where(actual: true).update(actual: false)
end

def refresh(model, params)
  entity = model.find_by(params)
  if entity.nil?
    entity = model.create!(params.merge(actual: true))
  else
    entity.actual = true
    entity.save!
  end

  entity
end

class SaveService
  def self.save(project, extracted_translations)
    # move to non-actual
    to_non_actual(project.sections)
    project.sections.each do |section|
      to_non_actual(section.files)
      section.files.each do |section_file|
        to_non_actual(section_file.translations)
      end
    end
    # move to non-actual

    # refresh
    extracted_translations.each do |extracted_translation|
      section = refresh(Section, project: project, name: extracted_translation[:section])
      extracted_translation[:files].each do |file|
        section_file = refresh(SectionFile, section: section, name: file[:name])
        file[:translations].each do |content|
          refresh(Translation, content: content, file: section_file)
        end
      end
    end
  end
end
