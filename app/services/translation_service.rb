# frozen_string_literal: true

class TranslationService
  def self.refresh(project, extractor_service = 'ExtractorService', save_service = 'SaveService')
    extracted_translations = extractor_service.constantize.extract(project.source_path)
    save_service.constantize.save(project, extracted_translations)
  end
end
