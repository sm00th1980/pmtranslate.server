# frozen_string_literal: true

require 'git'

class RepoService
  def self.refresh(project, clone_service = 'RepoService', update_service = 'RepoService')
    begin
      clone_service.constantize.clone(project)
    rescue
      update_service.constantize.update(project)
    end
  end

  def self.update(project)
    git = Git.open(project.path, log: Logger.new(STDOUT))
    git.pull('origin', project.branch)
  end

  def self.clone(project)
    Git.clone(project.url, project.name, path: Rails.root.join(project.dir))
  end
end
