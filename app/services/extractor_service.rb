# frozen_string_literal: true

def procdir(dir)
  Dir[File.join(dir, '**', '*')].reject { |p| File.directory? p }
end

def file_by_section(rows, section)
  rows.select { |row| row[:section] == section }.map { |row| row[:file] }
end

def translation_by_section_and_file(rows, section, file)
  rows.select { |row| row[:section] == section && row[:file] == file }.map { |row| row[:translations] }
end

class ExtractorService
  def self.extract(dir)
    rows = procdir(dir).map do |file|
      content = File.readlines file
      translations = Service::Parser.parse(content.join).uniq

      filename = file.split(dir.to_s).last[1..-1]

      section = filename.split('/').reject { |el| el == '' }[0..-2].join('/')
      section_file = filename.split('/').last

      !translations.empty? ? { section: section, file: section_file, translations: translations } : nil
    end.compact

    sections = rows.map { |el| el[:section] }.uniq
    sections_with_files = sections.map do |section|
      files_with_translations = file_by_section(rows, section).map do |file|
        translations = translation_by_section_and_file(rows, section, file).flatten.uniq
        { name: file, translations: translations }
      end

      { section: section, files: files_with_translations }
    end

    sections_with_files
  end
end
