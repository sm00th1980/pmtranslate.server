# frozen_string_literal: true

class TranslationWorker
  include Sidekiq::Worker

  def perform(refresh_repo_service = 'RepoService', refresh_translation_service = 'TranslationService')
    Project.all.each do |project|
      refresh_repo_service.constantize.refresh(project)
      refresh_translation_service.constantize.refresh(project)
    end
  end
end
