# frozen_string_literal: true

module Controllers
  module ApplicationHelper
    def root_page?
      [root_path].include? request.original_fullpath
    end

    def to_bool!(value)
      val_ = value.to_s.downcase rescue nil

      return true if val_.present? && val_ == 'true'
      return false if val_.present? && val_ == 'false'

      nil
    end

    def rows_count
      { '20' => 20, '30' => 30, 'all' => 'All' }
    end

    def per_page
      @per_page = rows_count.keys.map(&:to_s).include?(params[:rows_count].to_s) ? params[:rows_count].to_s : rows_count.keys.first.to_s
    end

    def page
      @page = params[:page].to_i.positive? ? params[:page].to_i : 1
    end

    def paginate_rows(rows)
      if per_page == 'all'
        Kaminari.paginate_array(rows.to_a).page(1).per(rows.to_a.size)
      else
        Kaminari.paginate_array(rows.to_a).page(page).per(@per_page)
      end
    end
  end
end
