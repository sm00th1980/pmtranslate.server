# frozen_string_literal: true

module Service
  class Parser
    def self.parse(input)
      input.scan(/[\s\{]t\(['"].+?['"]/).map { |el| el.split('t(').last.split(')').first.split('')[1..-2].join.force_encoding('utf-8') } rescue []
    end
  end
end
