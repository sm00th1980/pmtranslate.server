# frozen_string_literal: true

require 'rails_helper'

describe 'routing to session', type: :routing do
  it 'routing get /login' do
    expect(get: '/login').to route_to(controller: 'sessions', action: 'new')
  end

  it 'routing post /login' do
    expect(post: '/login').to route_to(controller: 'sessions', action: 'create')
  end

  it 'routing delete /logout' do
    expect(delete: '/logout').to route_to(controller: 'sessions', action: 'destroy')
  end
end
