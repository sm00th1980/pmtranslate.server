# frozen_string_literal: true

require 'rails_helper'

describe 'routing to api/v1', type: :routing do
  it 'routing get /api/v1/auth' do
    expect(get: '/api/v1/auth').to route_to(controller: 'api/v1/auth', action: 'index')
  end
end
