# frozen_string_literal: true

require 'rails_helper'

describe 'routing to api/v1', type: :routing do
  it 'routing get /api/v1/sections' do
    expect(get: '/api/v1/sections').to route_to(controller: 'api/v1/sections', action: 'index')
  end

  it 'routing get /api/v1/section_file/:id/translations' do
    expect(get: '/api/v1/section_file/123/translations').to route_to(controller: 'api/v1/section_file', action: 'translations', id: '123')
  end

  it 'routing get /api/v1/section/:id/files' do
    expect(get: '/api/v1/section/123/files').to route_to(controller: 'api/v1/section', action: 'files', id: '123')
  end

  it 'routing post /api/v1/translations/:id/toggle_approved' do
    expect(post: '/api/v1/translations/123/toggle_approved').to route_to(controller: 'api/v1/translations', action: 'toggle_approved', id: '123')
  end
end
