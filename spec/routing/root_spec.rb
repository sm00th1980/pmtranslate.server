# frozen_string_literal: true

require 'rails_helper'

describe 'routing to root', type: :routing do
  it 'routing get /' do
    expect(get: '/').to route_to(controller: 'root', action: 'index')
  end
end
