# frozen_string_literal: true

require 'rails_helper'

describe 'routing to section', type: :routing do
  it 'routing get /section/1/translations' do
    expect(get: '/section/1/translations').to route_to(controller: 'section', action: 'translations', id: '1')
  end

  it 'routing get /section/1/drop_non_actual' do
    expect(get: '/section/1/drop_non_actual').to route_to(controller: 'section', action: 'drop_non_actual', id: '1')
  end
end
