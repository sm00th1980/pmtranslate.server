# frozen_string_literal: true

require 'rails_helper'

describe 'routing to translation', type: :routing do
  it 'routing get /translation/1/approve' do
    expect(get: '/translation/1/approve').to route_to(controller: 'translation', action: 'approve', id: '1')
  end
end
