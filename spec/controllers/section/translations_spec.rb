# frozen_string_literal: true

require 'rails_helper'

describe SectionController, type: :controller do
  render_views

  describe 'GET section translations' do
    before(:each) do
      @user = User.all.sample
      @section = create(:section)

      @first_actual_section = Section.actual.first
      expect(@first_actual_section).to_not be_nil
    end

    it 'should redirect to new_session if non-auth user' do
      get :translations, params: { id: @section.id }
      expect(assigns(:section)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it 'should show translations in section' do
      sign_in @user
      get :translations, params: { id: @section.id }
      expect(assigns(:section)).to eq @section
      expect(response).to have_http_status(:success)
    end

    it 'should not show translations in section for not exist id' do
      sign_in @user

      get :translations, params: { id: -1 }

      expect(assigns(:section)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(section_translations_path(@first_actual_section))

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('section.failure.not_found')
    end
  end
end
