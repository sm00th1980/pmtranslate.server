# frozen_string_literal: true

require 'rails_helper'

describe SectionController, type: :controller do
  render_views

  describe 'GET drop_non_actual' do
    before(:each) do
      @user = User.all.sample
      @section = create(:section)

      expect(@section.non_actual_translations?).to be true

      @first_actual_section = Section.first
      expect(@first_actual_section).to_not be_nil
    end

    it 'should redirect to new_session if non-auth user' do
      get :drop_non_actual, params: { id: @section.id }
      expect(assigns(:section)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it 'should drop non actual translations' do
      sign_in @user
      get :drop_non_actual, params: { id: @section.id }
      expect(assigns(:section)).to eq @section

      expect(@section.reload.non_actual_translations?).to be false

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(section_translations_path(@section))

      expect(flash[:notice]).to eq I18n.t('section.drop_non_actual.success')
      expect(flash[:alert]).to be_nil
    end

    it 'should not drop non-actual-translations in section for not exist id' do
      sign_in @user

      get :drop_non_actual, params: { id: -1 }

      expect(assigns(:section)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(section_translations_path(@first_actual_section))

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('section.failure.not_found')
    end
  end
end
