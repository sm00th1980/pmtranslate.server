# frozen_string_literal: true

require 'rails_helper'

describe RootController, type: :controller do
  render_views

  # unsigned
  describe 'GET index for unsigned' do
    it 'should redirect to new_session if non-auth user' do
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  # user
  describe 'GET index for signed user' do
    before(:all) do
      @user = create(:user)
      expect(@user).to_not be_nil

      create(:section)
      @first_file = Section.actual.first.files.actual.first
    end

    it 'should redirect to search for signed user' do
      sign_in @user
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(section_file_translations_path(@first_file))

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to be_nil
    end
  end
end
