# frozen_string_literal: true

require 'rails_helper'

describe TranslationController, type: :controller do
  render_views

  describe 'update translation approve without auth' do
    before(:each) do
      @non_approved_translation = create(:non_approved_translation)

      @params = {
        id: @non_approved_translation.id,
        approve: true
      }
    end

    it 'should not update from false to true' do
      get :approve, params: @params
      expect(assigns(:translation)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)

      expect(@non_approved_translation.approved?).to be false
    end
  end

  describe 'update translation approve bu invalid id' do
    before(:each) do
      @user = User.all.sample

      @params = {
        id: -1,
        approve: true
      }
    end

    it 'should not update' do
      sign_in @user
      get :approve, params: @params
      expect(response).to have_http_status(:success)
      expect(assigns(:translation)).to be_nil

      json = JSON.parse(response.body)
      expect(json).to eq(
        'success' => false,
        'error' => I18n.t('translation.failure.not_found')
      )
    end
  end
end
