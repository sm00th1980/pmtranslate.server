# frozen_string_literal: true

require 'rails_helper'

describe TranslationController, type: :controller do
  render_views

  describe 'update translation approve' do
    before(:each) do
      @user = User.all.sample
      @non_approved_translation = create(:non_approved_translation)

      @params = {
        id: @non_approved_translation.id,
        approve: true
      }
    end

    it 'should update from false to true' do
      sign_in @user
      get :approve, params: @params
      expect(response).to have_http_status(:success)

      json = JSON.parse(response.body)
      expect(json).to eq('success' => true)

      expect(@non_approved_translation.reload.approved?).to be true
    end
  end

  describe 'update translation approve' do
    before(:each) do
      @user = User.all.sample
      @approved_translation = create(:approved_translation)

      @params = {
        id: @approved_translation.id,
        approve: false
      }
    end

    it 'should update from true to false' do
      sign_in @user
      get :approve, params: @params
      expect(response).to have_http_status(:success)

      json = JSON.parse(response.body)
      expect(json).to eq('success' => true)

      expect(@approved_translation.reload.approved?).to be false
    end
  end
end
