# frozen_string_literal: true

require 'rails_helper'

describe SectionFileController, type: :controller do
  render_views

  describe 'GET file translations' do
    before(:each) do
      @user = User.all.sample
      @file = create(:translation).file

      @first_file = SectionFile.first
      expect(@first_file).to_not be_nil
    end

    it 'should redirect to new_session if non-auth user' do
      get :translations, params: { id: @file.id }
      expect(assigns(:section)).to be_nil
      expect(assigns(:file)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it 'should show translations in file' do
      sign_in @user
      get :translations, params: { id: @file.id }
      expect(assigns(:file)).to eq @file
      expect(assigns(:section)).to eq @file.section
      expect(response).to have_http_status(:success)
    end

    it 'should not show translations in file for not exist id' do
      sign_in @user

      get :translations, params: { id: -1 }

      expect(assigns(:file)).to be_nil
      expect(assigns(:section)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(section_file_translations_path(@first_file))

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('file.failure.not_found')
    end
  end
end
