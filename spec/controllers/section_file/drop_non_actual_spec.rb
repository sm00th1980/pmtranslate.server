# frozen_string_literal: true

require 'rails_helper'

describe SectionFileController, type: :controller do
  render_views

  describe 'GET drop_non_actual' do
    before(:each) do
      @user = User.all.sample
      @file = create(:section_file)

      expect(@file.non_actual_translations?).to be true

      @first_file = SectionFile.first
      expect(@first_file).to_not be_nil
    end

    it 'should redirect to new_session if non-auth user' do
      get :drop_non_actual, params: { id: @file.id }
      expect(assigns(:file)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it 'should drop non actual translations' do
      sign_in @user
      get :drop_non_actual, params: { id: @file.id }
      expect(assigns(:file)).to eq @file

      expect(@file.non_actual_translations?).to be false

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(section_file_translations_path(@file))

      expect(flash[:notice]).to eq I18n.t('file.drop_non_actual.success')
      expect(flash[:alert]).to be_nil
    end

    it 'should not drop non-actual-translations in file for not exist id' do
      sign_in @user

      get :drop_non_actual, params: { id: -1 }

      expect(assigns(:file)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(section_file_translations_path(@first_file))

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('file.failure.not_found')
    end
  end
end
