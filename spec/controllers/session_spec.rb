# frozen_string_literal: true

require 'rails_helper'

describe SessionsController, type: :controller do
  render_views

  before(:each) do
    @request.env['devise.mapping'] = Devise.mappings[:user]
  end

  # describe "GET new" do
  #   it "should show login page" do
  #     get :new
  #     expect(response).to have_http_status(:success)
  #   end
  # end

  describe 'POST create' do
    before(:each) do
      @user = create(:user)
    end

    it 'should redirect to root page after success login' do
      post :create, params: params(@user)
      check_login
    end

    # invalid password
    it 'should redirect to login page after failure login' do
      post :create, params: params_with_incorrect_password(@user)
      check_not_login
    end

    # logout
    it 'should logout for user' do
      sign_in @user
      expect(warden.authenticated?(:user)).to be true
      delete :destroy
      check_not_login_after_logout
    end

    private

    def check_login
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to be_nil
      expect(warden.authenticated?(:user)).to be true
    end

    def check_not_login
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)

      expect(flash[:alert]).to eq I18n.t('devise.failure.user.invalid')
      expect(flash[:notice]).to be_nil
      expect(warden.authenticated?(:user)).to be false
    end

    def check_not_login_after_logout
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('devise.sessions.user.signed_out')
      expect(warden.authenticated?(:user)).to be false
    end

    def params(user)
      { username: user.email, password: user.password }
    end

    def params_with_incorrect_password(user)
      { username: user.email, password: (user.password + rand(100).to_s) }
    end
  end
end
