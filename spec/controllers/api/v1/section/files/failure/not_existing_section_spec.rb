# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::SectionController, type: :controller do
  render_views

  describe 'sections' do
    before(:each) do
      @section = create(:section)

      expect(@section.files.empty?).to be false

      @params = {
        id: -1
      }
    end

    it 'show all files' do
      get :files, params: @params
      expect(response).to be_successful
      expect(assigns(:section)).to be_nil

      json = JSON.parse(response.body)
      expect(json).to eq(
        'success' => false,
        'error' => I18n.t('section.failure.not_found')
      )
    end
  end
end
