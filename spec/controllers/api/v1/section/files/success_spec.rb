# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::SectionController, type: :controller do
  render_views

  describe 'sections' do
    before(:each) do
      @section = create(:section)

      expect(@section.files.empty?).to be false

      @params = {
        id: @section.id
      }
    end

    it 'show all files' do
      get :files, params: @params
      expect(response).to be_successful
      expect(assigns(:section)).to eq @section

      json = JSON.parse(response.body)
      expect(json).to eq(
        'success' => true,
        'files' => JSON.parse(@section.files.map(&:to_api).to_json)
      )
    end
  end
end
