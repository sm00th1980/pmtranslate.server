# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::SectionFileController, type: :controller do
  render_views

  describe 'sections' do
    before(:each) do
      @section_file = create(:section_file)

      expect(@section_file.translations.empty?).to be false

      @params = {
        id: @section_file.id
      }
    end

    it 'show all translations' do
      get :translations, params: @params
      expect(response).to be_successful
      expect(assigns(:section_file)).to eq @section_file

      json = JSON.parse(response.body)
      expect(json).to eq(
        'success' => true,
        'translations' => JSON.parse(@section_file.translations.map(&:to_api).to_json)
      )
    end
  end
end
