# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::AuthController, type: :controller do
  render_views

  describe 'get access_token by username and password' do
    before(:each) do
      @user = create(:user)

      expect(@user.password).to_not be_nil
      expect(@user.encrypted_password).to_not be_nil

      @params = {
        username: 'vfmdklvmdfkl',
        password: @user.password
      }
    end

    it 'show error is user not exists' do
      get :index, params: @params
      expect(response).to be_successful

      json = JSON.parse(response.body)
      expect(json).to eq(
        'success' => false,
        'error' => I18n.t('auth.failure.user_not_found')
      )
    end
  end
end
