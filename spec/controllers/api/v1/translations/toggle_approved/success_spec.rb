# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::TranslationsController, type: :controller do
  render_views

  describe 'update translation approve' do
    before(:each) do
      @user = create(:user)
      expect(@user.encrypted_password).to_not be_nil

      @non_approved_translation = create(:non_approved_translation)
      expect(@non_approved_translation.approved?).to be false

      @params = {
        id: @non_approved_translation.id,
        username: @user.email,
        access_token: Digest::MD5.hexdigest(@user.encrypted_password)
      }
    end

    it 'update from false to true' do
      post :toggle_approved, params: @params
      expect(response).to have_http_status(:success)

      @non_approved_translation.reload
      json = JSON.parse(response.body)
      expect(json).to eq(
        'success' => true,
        'translation' => JSON.parse(@non_approved_translation.to_api.to_json)
      )

      expect(@non_approved_translation.approved?).to be true
    end
  end

  describe 'update translation approve' do
    before(:each) do
      @user = create(:user)
      expect(@user.encrypted_password).to_not be_nil

      @approved_translation = create(:approved_translation)
      expect(@approved_translation.approved?).to be true

      @params = {
        id: @approved_translation.id,
        username: @user.email,
        access_token: Digest::MD5.hexdigest(@user.encrypted_password)
      }
    end

    it 'update from true to false' do
      post :toggle_approved, params: @params
      expect(response).to have_http_status(:success)

      @approved_translation.reload
      json = JSON.parse(response.body)
      expect(json).to eq(
        'success' => true,
        'translation' => JSON.parse(@approved_translation.to_api.to_json)
      )

      expect(@approved_translation.reload.approved?).to be false
    end
  end
end
