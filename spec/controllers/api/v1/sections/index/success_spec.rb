# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::SectionsController, type: :controller do
  render_views

  describe 'sections' do
    before(:each) do
      @section = create(:section)
      expect(Section.count).to be > 0
    end

    it 'show all sections' do
      get :index
      expect(response).to be_successful

      json = JSON.parse(response.body)
      expect(json).to eq(
        'success' => true,
        'sections' => JSON.parse(Section.all.map(&:to_api).to_json)
      )
    end
  end
end
