# frozen_string_literal: true

require 'rails_helper'

describe ExtractorService do
  describe 'valid input' do
    before(:each) do
      @dir = Rails.root.join('spec', 'fixtures', 'projects', 'gorko.r', 'src').to_s
    end

    it 'should extract all translations from dir' do
      expect(ExtractorService.extract(@dir)).to eq [{
        section: 'components/modules/profile/stats/ActionPanel',
        files: [{
          name: 'index.js',
          translations: ['Статистика 1', 'Статистика 2']
        }, {
          name: 'index2.js',
          translations: ['Статистика 3', 'Статистика 4']
        }]
      }]
    end
  end
end
