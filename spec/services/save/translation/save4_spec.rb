# frozen_string_literal: true

require 'rails_helper'

describe SaveService do
  before(:each) do
    @project = create(:project)
    @extracted_translation = {
      section: 'section',
      files: [{
        name: 'index.js',
        translations: ['Статистика']
      }]
    }

    @section = create(:actual_section, project: @project, name: @extracted_translation[:section])
    @section_file = create(:actual_section_file, section: @section, name: @extracted_translation[:files].first[:name])
    @translation = create(:actual_translation, file: @section_file, content: @extracted_translation[:files].first[:translations].first)
  end

  it 'should not create actual translation when it already created' do
    assert_difference 'Section.count', 0 do
      assert_difference 'SectionFile.count', 0 do
        assert_difference 'Translation.count', 0 do
          SaveService.save(@project, [@extracted_translation])

          expect(@section.reload.actual?).to be true
          expect(@section_file.reload.actual?).to be true
          expect(@translation.reload.actual?).to be true
        end
      end
    end
  end
end
