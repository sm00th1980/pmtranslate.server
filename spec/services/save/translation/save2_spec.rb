# frozen_string_literal: true

require 'rails_helper'

describe SaveService do
  before(:each) do
    @project = create(:project)
    @extracted_translation = {
      section: 'section',
      files: [{
        name: 'index.js',
        translations: ['Статистика']
      }]
    }

    @section = create(:actual_section, project: @project, name: @extracted_translation[:section])
  end

  it 'should save new translation and new secton-file when section already created' do
    assert_difference 'Section.count', 0 do
      assert_difference 'SectionFile.count', 1 do
        assert_difference 'Translation.count', 1 do
          SaveService.save(@project, [@extracted_translation])

          expect(@section.reload.actual?).to be true

          section_file = SectionFile.last
          expect(section_file).to_not be_nil
          expect(section_file.section).to eq @section
          expect(section_file.name).to eq @extracted_translation[:files].first[:name]
          expect(section_file.actual?).to be true

          translation = Translation.last
          expect(translation).to_not be_nil
          expect(translation.content).to eq @extracted_translation[:files].first[:translations].first
          expect(translation.file).to eq section_file
          expect(translation.actual?).to be true
        end
      end
    end
  end
end
