# frozen_string_literal: true

require 'rails_helper'

describe SaveService do
  before(:each) do
    @project = create(:project)
    @extracted_translation = {
      section: 'section_new',
      files: [{
        name: 'index_new.js',
        translations: ['Статистика']
      }]
    }

    @section = create(:actual_section, project: @project, name: 'section_old')
  end

  it 'should create new section' do
    assert_difference 'Section.count', 1 do
      assert_difference 'SectionFile.count', 1 do
        assert_difference 'Translation.count', 1 do
          SaveService.save(@project, [@extracted_translation])

          expect(@section.reload.actual?).to be false
          @section.files.each do |section_file|
            expect(section_file.actual?).to be false
            section_file.translations.each do |translation|
              expect(translation.actual?).to be false
            end
          end

          section = Section.last
          expect(section).to_not be_nil
          expect(section.project).to eq @project
          expect(section.name).to eq @extracted_translation[:section]
          expect(section.actual?).to be true

          section_file = SectionFile.last
          expect(section_file).to_not be_nil
          expect(section_file.section).to eq section
          expect(section_file.name).to eq @extracted_translation[:files].first[:name]
          expect(section_file.actual?).to be true

          translation = Translation.last
          expect(translation).to_not be_nil
          expect(translation.content).to eq @extracted_translation[:files].first[:translations].first
          expect(translation.file).to eq section_file
          expect(translation.actual?).to be true
        end
      end
    end
  end
end
