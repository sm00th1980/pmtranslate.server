# frozen_string_literal: true

require 'rails_helper'

describe SaveService do
  before(:each) do
    @project = create(:project)
    @extracted_translation = {
      section: 'section',
      files: [{
        name: 'index_new.js',
        translations: ['Статистика']
      }]
    }

    @section = create(:actual_section, project: @project, name: @extracted_translation[:section])
    @section_file = create(:non_actual_section_file, section: @section, name: @extracted_translation[:files].first[:name])
    expect(@section_file.actual?).to be false
    @old_translation_ids = @section_file.translations.map(&:id)
  end

  it 'should refresh non-actual-section-file' do
    assert_difference 'Section.count', 0 do
      assert_difference 'SectionFile.count', 0 do
        assert_difference 'Translation.count', 1 do
          SaveService.save(@project, [@extracted_translation])

          expect(@section.reload.actual?).to be true
          expect(@section_file.reload.actual?).to be true
          @old_translation_ids.each do |old_translation_id|
            expect(Translation.find_by(id: old_translation_id).actual?).to be false
          end

          translation = Translation.last
          expect(translation).to_not be_nil
          expect(translation.content).to eq @extracted_translation[:files].first[:translations].first
          expect(translation.file).to eq @section_file
          expect(translation.actual?).to be true
        end
      end
    end
  end
end
