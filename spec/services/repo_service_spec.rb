# frozen_string_literal: true

require 'rails_helper'

describe RepoService do
  describe 'clone first time' do
    before(:each) do
      @project = {}

      CloneService = class_spy('CloneService')
      allow(CloneService).to receive(:clone)

      UpdateService = class_spy('UpdateService')
      allow(UpdateService).to receive(:update)
    end

    after(:each) do
      Object.send(:remove_const, :CloneService)
      Object.send(:remove_const, :UpdateService)
    end

    it 'should call clone and not call update' do
      RepoService.refresh(@project, 'CloneService', 'UpdateService')

      expect(CloneService).to have_received(:clone).with(@project)
      expect(UpdateService).to_not have_received(:clone)
    end
  end

  describe 'update second time' do
    before(:each) do
      @project = {}

      CloneService = class_spy('CloneService')
      allow(CloneService).to receive(:clone).and_throw('path non empty')

      UpdateService = class_spy('UpdateService')
      allow(UpdateService).to receive(:update)
    end

    after(:each) do
      Object.send(:remove_const, :CloneService)
      Object.send(:remove_const, :UpdateService)
    end

    it 'should call clone with exception and then call update' do
      RepoService.refresh(@project, 'CloneService', 'UpdateService')

      expect(CloneService).to have_received(:clone).with(@project)
      expect(UpdateService).to have_received(:update).with(@project)
    end
  end
end
