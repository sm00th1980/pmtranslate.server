# frozen_string_literal: true

require 'rails_helper'

describe TranslationService do
  before(:each) do
    @project = instance_double('project', source_path: 'source_path')

    ExtractorServiceMock = class_spy('ExtractorServiceMock')
    allow(ExtractorServiceMock).to receive(:extract).and_return([1, 2, 3])

    SaveServiceMock = class_spy('SaveServiceMock')
    allow(SaveServiceMock).to receive(:save)
  end

  after(:each) do
    Object.send(:remove_const, :ExtractorServiceMock)
    Object.send(:remove_const, :SaveServiceMock)
  end

  it 'should call extract and then save' do
    TranslationService.refresh(@project, 'ExtractorServiceMock', 'SaveServiceMock')

    expect(ExtractorServiceMock).to have_received(:extract).with(@project.source_path)
    expect(SaveServiceMock).to have_received(:save).with(@project, [1, 2, 3])
  end
end
