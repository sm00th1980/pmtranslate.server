# frozen_string_literal: true

require 'rails_helper'

describe TranslationWorker, type: :worker do
  describe 'refresh repo then refresh all translations' do
    before(:each) do
      Project.delete_all
      @project = create(:project)

      RefreshRepoService = class_spy('RefreshRepoService')
      allow(RefreshRepoService).to receive(:refresh)

      RefreshTranslationService = class_spy('RefreshTranslationService')
      allow(RefreshTranslationService).to receive(:refresh)
    end

    after(:each) do
      Object.send(:remove_const, :RefreshRepoService)
      Object.send(:remove_const, :RefreshTranslationService)
    end

    it 'refresh repo then refresh all translations' do
      Sidekiq::Testing.inline! do
        expect { TranslationWorker.perform_async('RefreshRepoService', 'RefreshTranslationService') }.not_to raise_error

        expect(RefreshRepoService).to have_received(:refresh).with(@project)
        expect(RefreshTranslationService).to have_received(:refresh).with(@project)
      end
    end
  end
end
