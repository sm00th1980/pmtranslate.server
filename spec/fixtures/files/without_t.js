import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { itemsPropTypes } from 'component/Select/';
import omit from 'lodash/omit';
import FilterRecord from 'store/records/components/modules/profile/stats/filter';
import Form from './Control';

class Control extends Component {
	constructor(props) {
		super(props);
		this.state = props.initialState.toJS();

		// autobinding
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	onChange(key, value) {
		this.setState({ [`${key}`]: value });
	}

	onSubmit() {
		this.props.showState(omit(this.state, 'selectData'));
	}

	render() {
		return (
			<div className="flex-wrap-bg typeSeparate">
				<div className="flex-wrap">
					<Form
						onChange={this.onChange}
						state={this.state}
						items={this.props.selectData}
						onSubmit={this.onSubmit}
					/>
				</div>
			</div>
		);
	}
}

Control.propTypes = {
	showState: PropTypes.func.isRequired,
	initialState: PropTypes.instanceOf(FilterRecord).isRequired,
	selectData: itemsPropTypes.isRequired,
};

export default Control;
