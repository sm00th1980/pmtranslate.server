import React from 'react';

export default () => (
	<div className="flex-wrap-bg typeActionPanel ** js-action-panel">
		<div className="flex-wrap">
			<div className="actionPanel-l" />
			<div className="actionPanel-title">{ t('Статистика 1') }</div>
			<div className="actionPanel-r" />
			<div>{ t('Статистика 2') }</div>
		</div>
	</div>
);
