import React from 'react';

export default () => (
	<div className="flex-wrap-bg typeActionPanel ** js-action-panel">
		<div className="flex-wrap">
			<div className="actionPanel-l" />
			<div className="actionPanel-title">{ t('Статистика 3') }</div>
			<div className="actionPanel-r" />
			<div>{ t('Статистика 4') }</div>
		</div>
	</div>
);
