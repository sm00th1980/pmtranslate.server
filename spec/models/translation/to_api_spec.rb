# frozen_string_literal: true

require 'rails_helper'

describe 'Translation', type: :model do
  before(:each) do
    @translation = create(:translation)
  end

  it 'return correct to_api' do
    expect(@translation.to_api).to eq(
      id: @translation.id,
      content: @translation.content,
      file_id: @translation.file_id,
      approved: @translation.approved,
      repeat_count: @translation.repeat_count,
      created_at: @translation.created_at.to_i
    )
  end
end
