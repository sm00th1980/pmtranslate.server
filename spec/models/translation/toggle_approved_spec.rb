# frozen_string_literal: true

require 'rails_helper'

describe Translation, type: :model do
  describe 'toggle translation from false to true' do
    before(:each) do
      @translation = create(:translation, approved: false)
      expect(@translation.approved?).to be false
    end

    it 'toggle approved' do
      @translation.toggle_approved!
      @translation.reload
      expect(@translation.approved?).to be true
    end
  end

  describe 'toggle approved from true to false' do
    before(:each) do
      @translation = create(:translation, approved: true)
      expect(@translation.approved?).to be true
    end

    it 'toggle approved' do
      @translation.toggle_approved!
      @translation.reload
      expect(@translation.approved?).to be false
    end
  end
end
