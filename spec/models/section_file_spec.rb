# frozen_string_literal: true

require 'rails_helper'

describe SectionFile, type: :model do
  describe 'section file with non-actual-translations' do
    before(:each) do
      @section_file = create(:section_file)
      expect(@section_file.translations.non_actual.count > 0).to be true
    end

    it 'should has non_actual_translations? => true if file has non-actual-translations' do
      expect(@section_file.non_actual_translations?).to eq true
    end
  end

  describe 'section file without non-actual-translations' do
    before(:each) do
      @section_file = create(:section_file)
      @section_file.translations.non_actual.destroy_all
      expect(@section_file.translations.non_actual.count == 0).to be true
    end

    it 'should has non_actual_translations? => false if file has not non-actual-translations' do
      expect(@section_file.non_actual_translations?).to eq false
    end
  end
end
