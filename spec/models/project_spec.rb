# frozen_string_literal: true

require 'rails_helper'

describe Project, type: :model do
  before(:each) do
    @project = create(:project, dir: File.join('tmp', 'projects'), subdir: 'src')
  end

  it 'should has correct source_path' do
    expect(@project.source_path).to eq Rails.root.join(@project.dir, @project.name, @project.subdir)
  end

  it 'should has correct path' do
    expect(@project.path).to eq Rails.root.join(@project.dir, @project.name)
  end
end
