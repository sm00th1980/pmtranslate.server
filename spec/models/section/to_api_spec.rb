# frozen_string_literal: true

require 'rails_helper'

describe 'Section', type: :model do
  before(:each) do
    @section = create(:section, name: '01___test')
    expect(@section.files.empty?).to be false
  end

  it 'has return correct to_api' do
    expect(@section.to_api).to eq(
      id: @section.id,
      name: 'test',
      files_count: @section.files.size,
      translations_count: @section.files.map(&:translations_total).sum
    )
  end
end
