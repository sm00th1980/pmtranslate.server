# frozen_string_literal: true

require 'rails_helper'

describe Translation, type: :model do
  describe 'One repeat' do
    before(:each) do
      @translation = create(:translation)
    end

    it 'should has repeat_count eq 1 for one repeat' do
      expect(@translation.repeat_count).to eq 1
    end
  end

  describe 'Many repeat in different section-files' do
    before(:each) do
      section = create(:section)

      section_file1 = create(:section_file, section: section)
      section_file2 = create(:section_file, section: section)

      @translation1 = create(:translation, file: section_file1, content: 'content')
      @translation2 = create(:translation, file: section_file2, content: 'content')
    end

    it 'should has repeat_count eq 2 for two repeat' do
      expect(@translation1.repeat_count).to eq 2
    end

    it 'should has repeat_count eq 2 for two repeat' do
      expect(@translation2.repeat_count).to eq 2
    end
  end
end
