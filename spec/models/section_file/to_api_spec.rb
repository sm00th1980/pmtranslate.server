# frozen_string_literal: true

require 'rails_helper'

describe SectionFile, type: :model do
  before(:each) do
    @section_file = create(:section_file)
    expect(@section_file.translations.empty?).to be false
  end

  it 'has return correct to_api' do
    expect(@section_file.to_api).to eq(
      id: @section_file.id,
      name: @section_file.name,
      section_id: @section_file.section_id,
      translations: @section_file.translations.map(&:to_api)
    )
  end
end
