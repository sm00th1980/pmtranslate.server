# frozen_string_literal: true

require 'rails_helper'

describe Section, type: :model do
  describe 'section with non-actual-translations' do
    before(:each) do
      @section = create(:section)
      expect(@section.files.select(&:non_actual_translations?).count > 0).to be true
    end

    it 'should has non_actual_translations? =. true if section has non-actual-translations' do
      expect(@section.non_actual_translations?).to eq true
    end
  end

  describe 'section without non-actual-translations' do
    before(:each) do
      @section = create(:section)
      @section.files.select(&:non_actual_translations?).each(&:destroy)
      expect(@section.reload.files.select(&:non_actual_translations?).count == 0).to be true
    end

    it 'should has non_actual_translations? => false if section has not non-actual-translations' do
      expect(@section.non_actual_translations?).to eq false
    end
  end
end
