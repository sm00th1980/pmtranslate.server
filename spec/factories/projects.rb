# frozen_string_literal: true

FactoryBot.define do
  factory :project do
    sequence(:name) { |n| "name_of_project_#{n}" }
    dir { File.join('tmp', 'projects') }
    subdir { 'src' }
    branch { 'master' }
    url { 'git@bitbucket.org:pmcloud/gorko.r.git' }
  end
end
