# frozen_string_literal: true

FactoryBot.define do
  factory :section do
    sequence(:name) { |n| "name_of_section_#{n}" }
    project

    factory :actual_section do
      actual { true }
    end

    factory :non_actual_section do
      actual { false }
    end

    after(:create) do |section|
      2.times do
        create(:actual_section_file, section: section)
        create(:non_actual_section_file, section: section)
      end
    end
  end
end
