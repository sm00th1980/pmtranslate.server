# frozen_string_literal: true

FactoryBot.define do
  factory :section_file do
    sequence(:name) { |n| "name_of_section_file_#{n}" }
    section

    factory :actual_section_file do
      actual { true }
    end

    factory :non_actual_section_file do
      actual { false }
    end

    after(:create) do |section_file|
      2.times do
        create(:actual_translation, file: section_file)
        create(:non_actual_translation, file: section_file)
      end
    end
  end
end
