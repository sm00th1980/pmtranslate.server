# frozen_string_literal: true

FactoryBot.define do
  factory :translation do
    sequence(:content) { |n| "content_for_translation_#{n}" }
    file_id { create(:section_file).id }

    factory :actual_translation do
      actual { true }
    end

    factory :non_actual_translation do
      actual { false }
    end

    factory :non_approved_translation do
      approved { false }
    end

    factory :approved_translation do
      approved { true }
    end
  end
end
