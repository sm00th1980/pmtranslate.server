# frozen_string_literal: true

require 'rails_helper'

describe Service::Parser do
  describe 'valid input' do
    before(:each) do
      @input = fixture_file_upload('files/index.js', 'text/html').read
    end

    it 'should parse from source' do
      expect(Service::Parser.parse(@input)).to eq ['Статистика']
    end
  end

  describe 'without t()' do
    before(:each) do
      @input = fixture_file_upload('files/without_t.js', 'text/html').read
    end

    it 'should parse from source' do
      expect(Service::Parser.parse(@input)).to eq []
    end
  end

  describe 'without blank before t(' do
    before(:each) do
      @input1 = "const weddingDate = data.wedding_date ? momentjs(data.wedding_date, 'YYYY-MM-DD').format('LL') : null;"
      @input2 = "columnNames: profileStats.get('columnNames'),"

      @input3 = "<Check value={t('Посмотрели контакты')} checked={state.viewed_contacts} onChange={checkChange(onChange, 'viewed_contacts')} />"
    end

    it 'should parse from source' do
      expect(Service::Parser.parse(@input1)).to eq []
    end

    it 'should parse from source' do
      expect(Service::Parser.parse(@input2)).to eq []
    end

    it 'should parse from source' do
      expect(Service::Parser.parse(@input3)).to eq ['Посмотрели контакты']
    end
  end

  describe 'with component inside t()' do
    before(:each) do
      @input = "<Locale template={component => t('{value} заблокировано', {value: component})}>"
    end

    it 'should parse from source' do
      expect(Service::Parser.parse(@input)).to eq ['{value} заблокировано']
    end
  end

  describe 'invalid input' do
    it 'should parse invalid inputs' do
      [nil, 1, [], [nil], ''].each do |invalid_input|
        expect(Service::Parser.parse(invalid_input)).to eq []
      end
    end
  end
end
